# Laravel boilerplate

Laravel project for setting up a simple json api.

## Getting Started

### Prerequisites:

- [php](http://php.net/)
- [composer](https://getcomposer.org/)
- [laravel](https://laravel.com/)

## Project info

### Setup info

TODO add

### Setup Passport oauth2 client

[Laravel docs](https://laravel.com/docs/5.7/passport#password-grant-tokens) for password grant tokens setup

1. Create client
```
php artisan passport:client --password
```
2. Create user via tinker
```
php artisan tinker
```
```
>>> $user = new User()
>>> $user->name = 'tim'
>>> $user->email = 'tim@vanginderen.be'
>>> $user->password = Hash::make('my_password')
>>> $user->save
```

3. Test if everything works by requesting a token
```
curl -X POST \
  http://localhost:8000/oauth/token \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
    "grant_type" : "password",
    "client_id" : "4",
    "client_secret" : "3ziwkC8rexJfapjZ0dzjjmRnWqdfaPlf8Z7cs",
    "username" : "tim@vanginderen.be",
    "password" : "my_password",
    "scope" : ""
}'
```
4. (Optional) test register via api
```
curl -X POST \
  http://localhost:8000/api/register \
  -H 'Accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
    "client_id" : "4",
    "client_secret" : "3ziwkC8rexJfapjZ0dzjjmRnWqdfaPlf8Z7cs",
    "email" : "tim2@vanginderen.be",
    "password" : "my_second_password",
    "password_confirmation" : "my_second_password",
    "name": "tim"
}'
```